<?php
namespace Elogic\HelloWorld\Controller\Elogic;

use Codeception\Util\Template;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
class First extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    public function execute()
    {
        /** @var First $resultPage */
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        /** @var Template $block */
        $block = $page->getLayout()->getBlock('elogic.helloworld.layout.example');
//        echo $_product->getEntityId();
//        echo '<br />';
//        echo $_product->getName();
        $block->setData('custom', 'Data');
        return $page;
    }
}
