<?php
namespace Elogic\HelloWorld\Block;
class HelloWorld extends \Magento\Framework\View\Element\Template
{
    protected $_productRepository;
    protected $_categoryFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ){
        $this->_productRepository = $productRepository;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context, $data);
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }
    public function  getCategoryName()
    {
        $categoryId = '3';
        $category = $this->_categoryFactory->create()->load($categoryId);
        return $category->getName();
    }
}
