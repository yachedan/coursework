<?php
declare(strict_types=1);

namespace Elogic\HelloWorld\ViewModel;

use Elogic\HelloWorld\Helper\Data;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class AuthorViewModel implements ArgumentInterface
{
    private $authorHelper;

    /**
     * AuthorViewModel constructor.
     * @param Data $authorHelper
     */
    public function __construct(
        Data $authorHelper
    ) {
        $this->authorHelper = $authorHelper;
    }
    public function isAuthorModelEnabled() : bool
    {
        return $this->authorHelper->isEnabled();
    }
}
