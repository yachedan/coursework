<?php
declare(strict_types=1);

namespace Elogic\Author\Api;

use Elogic\Author\Api\Data\AuthorInterface;
use Elogic\Author\Api\Data\AuthorSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

interface AuthorRepositoryInterface
{

    /**
     * @param int $id
     * @return AuthorInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id) : AuthorInterface;

    /**
     * @param AuthorInterface $author
     * @return void
     */
    public function save(AuthorInterface $author) : void;

    /**
     * @param AuthorInterface $author
     * @return  void
     */
    public function delete(AuthorInterface $author) : void;

    /**
     * @param int $id
     * @return void
     */
    public function deleteById(int $id) : void;

}
