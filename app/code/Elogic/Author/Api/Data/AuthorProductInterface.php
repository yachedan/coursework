<?php
declare(strict_types=1);
namespace Elogic\Author\Api\Data;

interface AuthorProductInterface
{
    const ID = "_id";
    const PRODUCT_ID = "product_id";
    const AUTHOR_ID = "author_id";

    /**
     * @return int|null
     */
    public function getProductId() : ?int;
    /**
     * @return int|null
     */
    public function getAuthorId() : ?int;

    /**
     * @param int $id
     * @return AuthorProductInterface
     */
    public function setAuthorId(int $id) : AuthorProductInterface;
    /**
     * @param int $id
     * @return AuthorProductInterface
     */
    public function setProductId(int $id) : AuthorProductInterface;
}
