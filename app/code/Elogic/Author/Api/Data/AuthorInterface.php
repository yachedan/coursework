<?php
declare(strict_types=1);
namespace Elogic\Author\Api\Data;

interface AuthorInterface
{
    const AUTHOR_ID = "_id";
    const FULL_NAME = "full_name";
    const LIVING_YEARS = "living_years";
    const CREATION_TIME = "creation_time";
    const  UPDATE_TIME = "update_time";

    /**
     * @return int|null
     */
    public function getId() : ?int;

    /**
     * @return string|null
     */
    public function getFullName() : ?string;

    /**
     * @return string|null
     */
    public function getLivingYears() : ?string;

    /**
     * @return string|null
     */
    public function getCreationTime() : ?string;

    /**
     * @return string|null
     */
    public function getUpdateTime() : ?string;

    /**
     * @param int $id
     * @return AuthorInterface
     */
    public function setId(int $id) : AuthorInterface;

    /**
     * @param string $name
     * @return AuthorInterface
     */
    public function setFullName(string $name) : AuthorInterface;

    /**
     * @param string $name
     * @return AuthorInterface
     */
    public function setLivingYears(string $name) : AuthorInterface;

    /**
     * @param string $creationTime
     * @return AuthorInterface
     */
    public function setCreationTime(string $creationTime) : AuthorInterface;

    /**
     * @param string $updateTime
     * @return AuthorInterface
     */
    public function setUpdateTime(string $updateTime) : AuthorInterface;
}
