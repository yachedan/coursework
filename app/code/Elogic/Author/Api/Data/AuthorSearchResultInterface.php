<?php

namespace Elogic\Author\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface AuthorSearchResultInterface extends SearchResultsInterface
{

    /**
     * @return \Elogic\Author\Api\Data\AuthorInterface[]
     */
    public function getItems();

    /**
     * @param \Elogic\Author\Api\Data\AuthorInterface[] $items
     * @return void;
     */
    public function setItems(array  $items);
}
