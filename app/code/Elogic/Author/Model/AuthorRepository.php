<?php
declare(strict_types=1);
namespace Elogic\Author\Model;

use Elogic\Author\Api\AuthorRepositoryInterface;
use Elogic\Author\Api\Data\AuthorInterface;
use Elogic\Author\Api\Data\AuthorSearchResultInterface;
use Elogic\Author\Api\Data\AuthorSearchResultInterfaceFactory;
use Elogic\Author\Model\AuthorFactory as AuthorFactory;
use Elogic\Author\Model\ResourceModel\Author\Collection;
use Elogic\Author\Model\ResourceModel\Author\CollectionFactory as AuthorCollectionFactory;
use Elogic\Author\Model\ResourceModel\AuthorFactory as AuthorResourceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\NoSuchEntityException;

class AuthorRepository implements AuthorRepositoryInterface
{
    /**
     * @var AuthorResourceFactory
     */
    private $authorResourceFactory;
    /**
     * @var AuthorFactory
     */
    private $authorFactory;

    /**
     * @var AuthorCollectionFactory
     */
    private $authorCollectionFactory;

    /**
     * @var AuthorSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * AuthorRepository constructor.
     * @param AuthorFactory $authorFactory
     * @param AuthorResourceFactory $authorResourceFactory
     */
    public function __construct(
        AuthorFactory $authorFactory,
        AuthorResourceFactory $authorResourceFactory,
        AuthorCollectionFactory $authorCollectionFactory,
        AuthorSearchResultInterfaceFactory $authorSearchResultInterfaceFactory
    ) {
        $this->authorFactory = $authorFactory;
        $this->authorResourceFactory = $authorResourceFactory;
        $this->authorCollectionFactory = $authorCollectionFactory;
        $this->searchResultFactory = $authorSearchResultInterfaceFactory;
    }

    /**
     * @param int $id
     * @return Author
     * @throws NoSuchEntityException
     */
    public function getById(int $id) : AuthorInterface
    {
        $author = $this->authorFactory->create();
        $this->authorResourceFactory->create()->load($author, $id);
        if (!$author->getId()) {
            throw new NoSuchEntityException(__('There is no Author with this ID'));
        }
        return $author;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return AuthorSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria) : AuthorSearchResultInterface
    {
        $collection = $this->authorCollectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     */
    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $field[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array) $searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }
    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }
    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
    /**
     * @param AuthorInterface $author
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @return  void
     */
    public function save(AuthorInterface $author): void
    {
        $this->authorResourceFactory->create()->save($author);
    }

    /**
     * @param AuthorInterface $author
     * @throws \Exception
     * @return void
     */
    public function delete(AuthorInterface $author) : void
    {
        $this->authorResourceFactory->create()->delete($author);
    }

    /**
     * @param int $id
     * @throws NoSuchEntityException
     * @throws \Exception
     * @return void
     */
    public function deleteById(int $id) : void
    {
        $author = $this->getById($id);
        $this->delete($author);
    }
}
