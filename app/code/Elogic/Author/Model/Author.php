<?php

namespace Elogic\Author\Model;

use Elogic\Author\Api\Data\AuthorInterface;
use Elogic\Author\Model\ResourceModel\Author as AuthorResource;
use Magento\Framework\Model\AbstractModel;

class Author extends AbstractModel implements AuthorInterface
{
    protected $_eventPrefix = "author";

    protected $_eventObject = "author";
    public function _construct()
    {
        $this->_init(AuthorResource::class);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->getData(self::AUTHOR_ID);
    }

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->getData(self::FULL_NAME);
    }

    /**
     * @return string|null
     */
    public function getLivingYears(): ?string
    {
        return $this->getData(self::LIVING_YEARS);
    }

    /**
     * @return string|null
     */
    public function getCreationTime(): ?string
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime(): ?string
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @param int $id
     * @return AuthorInterface
     */
    public function setId($id): AuthorInterface
    {
        return $this->setData(self::AUTHOR_ID, $id);
    }

    /**
     * @param string $name
     * @return AuthorInterface
     */
    public function setFullName(string $name): AuthorInterface
    {
        return $this->setData(self::FULL_NAME, $name);
    }

    /**
     * @param string $years
     * @return AuthorInterface
     */
    public function setLivingYears(string $years): AuthorInterface
    {
        return $this->setData(self::LIVING_YEARS, $years);
    }

    /**
     * @param string $creationTime
     * @return AuthorInterface
     */
    public function setCreationTime(string $creationTime) : AuthorInterface
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * @param string $updateTime
     * @return AuthorInterface
     */
    public function setUpdateTime(string $updateTime): AuthorInterface
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }
}
