<?php
namespace Elogic\Author\Model\ResourceModel\Author;

use Elogic\Author\Model\ResourceModel\Author as AuthorResource;
use \Elogic\Author\Model\Author;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Author::class, AuthorResource::class);
    }
}
