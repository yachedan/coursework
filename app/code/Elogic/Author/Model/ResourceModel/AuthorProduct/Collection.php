<?php


namespace Elogic\Author\Model\ResourceModel\AuthorProduct;


use Elogic\Author\Model\Author;
use Elogic\Author\Model\ResourceModel\AuthorProduct as AuthorProductResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Author::class, AuthorProductResource::class);
    }
}
