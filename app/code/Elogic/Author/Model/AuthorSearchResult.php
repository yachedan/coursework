<?php

namespace Elogic\Author\Model;

use Elogic\Author\Api\Data\AuthorSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class AuthorSearchResult extends SearchResults implements AuthorSearchResultInterface
{
}
