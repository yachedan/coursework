<?php

namespace Elogic\Author\Model\Attribute\Source;

use Elogic\Author\Api\AuthorRepositoryInterface;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Author extends AbstractSource
{
    /**
     * @var AuthorRepositoryInterface
     */
    private $authorRepository;

    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [];
            foreach ($this->authorRepository->getList(null) as $author) {
                $this->_options[] =  ['label' => __($author->getFullName()), 'value' => $author->getFullName()];
            }
        }
        /*if (!$this->_options) {
            $this->_options = [
                ['label' => __('Cotton'), 'value' => 'cotton'],
                ['label' => __('Leather'), 'value' => 'leather'],
                ['label' => __('Silk'), 'value' => 'silk'],
                ['label' => __('Denim'), 'value' => 'denim'],
                ['label' => __('Fur'), 'value' => 'fur'],
                ['label' => __('Wool'), 'value' => 'wool'],
            ];
        }*/
        return $this->_options;
    }
}
