<?php
namespace Elogic\Author\Controller\Adminhtml\Index;

use Elogic\Author\Api\AuthorRepositoryInterface as AuthorRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Elogic_Author::author';

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var \Elogic\Author\Api\AuthorRepositoryInterface
     */
    private $authorRepository;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param AuthorRepository $authorRepository
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        AuthorRepository $authorRepository
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->authorRepository = $authorRepository;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData(
                [
                    'messages' => [__('Please correct the data sent.')],
                    'error' => true,
                ]
            );
        }
        foreach (array_keys($postItems) as $authorId) {
            /** @var \Elogic\Author\Model\Author $author */
            $author = $this->authorRepository->getById($authorId);
            try {
                $author->setData(array_merge($author->getData(), $postItems[$authorId]));
                $this->authorRepository->save($author);
            } catch (\Exception $e) {
                $messages[] = "[Author ID: {$authorId}]  {$e->getMessage()}";
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
