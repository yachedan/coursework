<?php
namespace  Elogic\Author\Block;

use Elogic\Author\Model\AuthorFactory;
use Elogic\Author\Model\AuthorRepository;
use Elogic\Author\Model\ResourceModel\Author\Collection;
use Elogic\Author\Model\ResourceModel\Author\CollectionFactory as AuthorCollectionFactory;
use Elogic\Author\Model\ResourceModel\AuthorFactory as AuthorResourceFactory;
use Magento\Framework\View\Element\Template;

class Authors extends \Magento\Framework\View\Element\Template
{
    private $authorResourceFactory;
    private $authorFactory;
    private $authorColllectionFactory;
    private $authorRepository;

    public function __construct(
        Template\Context $context,
        AuthorFactory $authorFactory,
        AuthorResourceFactory $authorResourceFactory,
        AuthorCollectionFactory $authorColllectionFactory,
        AuthorRepository $authorRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->authorFactory = $authorFactory;
        $this->authorResourceFactory = $authorResourceFactory;
        $this->authorColllectionFactory = $authorColllectionFactory;
        $this->authorRepository = $authorRepository;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAuthor()
    {
//        $author = $this->authorFactory->create();
//        $this->authorResourceFactory->create()->load($author, 1);
//        return $author;
        return $this->authorRepository->getById(1);
        //return $this->author->load(1);
    }
    public function getAuthors()
    {
        /** @var Collection $collection */
        $collection = $this->authorColllectionFactory->create();
        $collection->getSelect();
        //$collection->setPageSize(2)->setCurPage(1);
        //$collection->getSelect()->limit(2,1);
        //echo $collection->getSelect()->__toString();
//        echo count($collection->getItems());
//        die;
        return $collection;
    }
}
