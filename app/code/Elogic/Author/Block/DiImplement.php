<?php
namespace Elogic\Author\Block;

use Elogic\Author\Api\Data\AuthorInterface;
use Elogic\Author\Model\Author;
use Elogic\Author\Model\ResourceModel\Author as ResourceAuthor;
use Magento\Framework\View\Element\Template;

class DiImplement extends Template
{
    /**
     * @var Author
     */
    private $author;
    /**
     * @var ResourceAuthor
     */
    private $authorResource;

    public function __construct(Template\Context $context, AuthorInterface $author, ResourceAuthor $authorResource, array $data = [])
    {
        parent::__construct($context, $data);
        $this->author = $author;
        $this->authorResource = $authorResource;
    }

    public function getAuthor()
    {
        return $this->author->load(2);
    }
}
